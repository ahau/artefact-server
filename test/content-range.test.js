const test = require('tape')
const path = require('path')
const fs = require('fs')
const axios = require('axios')
const ArtefactServer = require('../')

const filePath = path.join(__dirname, '../README.md')
const file = fs.readFileSync(filePath, 'utf-8')

test('Content-Range', async t => {
  const server = await ArtefactServer({ dev: true })

  const blobDetails = await server.store.addFile(filePath, { targetFileName: '1234-5678' })
  const {
    driveAddress: drive,
    fileName: blobId,
    fileEncryptionKey: readKey
  } = blobDetails

  const url = new URL(`http://${server.host}:${server.port}/drive/${drive}/blob/${blobId}`)
  url.searchParams.append('readKey', readKey)
  url.searchParams.append('mimeType', 'application/text')

  await noRange()
  await rangeStart()
  await rangeStartEnd()
  await rangeStartEnd2()

  async function noRange () {
    t.comment('no range')
    const res = await axios.get(url.href)
    t.equal(res.status, 200, 'http statusCode 200')
    t.equal(parseInt(res.headers['content-length']), file.length, 'has header: content-length')
    t.equal(res.headers['accept-ranges'], 'bytes', 'has header: accept-ranges')
    t.equal(res.headers['content-range'], undefined, 'no header: content-range')
    t.equal(res.data, file, 'gets whole file')
  }

  async function rangeStart () {
    t.comment('range: start-')
    const start = 94
    const opts = {
      headers: {
        Range: `bytes=${start}-`
      }
    }

    const res = await axios.get(url.href, opts)

    t.equal(res.status, 206, 'http statusCode 206')
    t.match(res.headers['content-range'], new RegExp(`^bytes ${start}-${file.length - 1}/\\d+$`), 'has header: content-range')
    t.equal(parseInt(res.headers['content-length']), file.length - start, 'has header: content-length')
    // last byte = file.length - 1
    // first byte = start
    // total bytes = (file.length - 1) - (start) + 1
    // the plus 1 on end is becuase these ranges are inclusive

    t.equal(res.data, file.slice(start), 'gets slice of data')
  }

  async function rangeStartEnd () {
    t.comment('range: start-end')
    const start = 0
    const end = 49
    const opts = {
      headers: {
        Range: `bytes=${start}-${end}`
      }
    }

    const res = await axios.get(url.href, opts)

    t.equal(res.status, 206, 'http statusCode 206')
    t.equal(res.headers['accept-ranges'], 'bytes', 'has header: accept-ranges')
    t.match(res.headers['content-range'], new RegExp(`^bytes ${start}-${end}/\\d+$`), 'has header: content-range')
    t.equal(parseInt(res.headers['content-length']), end - start + 1, 'has header: content-length')

    t.equal(res.data, file.slice(start, end + 1), 'gets slice of data')
  }

  async function rangeStartEnd2 () {
    t.comment('range: start-end')
    const start = 94
    const end = 140
    const opts = {
      headers: {
        Range: `bytes=${start}-${end}`
      }
    }

    const res = await axios.get(url.href, opts)

    t.equal(res.status, 206, 'http statusCode 206')
    t.equal(res.headers['accept-ranges'], 'bytes', 'has header: accept-ranges')
    t.match(res.headers['content-range'], new RegExp(`^bytes ${start}-${end}/\\d+$`), 'has header: content-range')
    t.equal(parseInt(res.headers['content-length']), end - start + 1, 'has header: content-length')

    t.equal(res.data, file.slice(start, end + 1), 'gets slice of data')
  }

  server.close()
  t.end()
})
