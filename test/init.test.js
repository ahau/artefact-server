const test = require('tape')
const ArtefactServer = require('../')

test('init ArtefactServer', t => {
  console.time('ArtefactServer startup')

  ArtefactServer({ dev: true })
    .then(server => {
      t.ok(server, 'starts server!')
      console.timeEnd('ArtefactServer startup')

      server.close()
        .then(t.end)
        .catch(err => {
          t.error(err)
          t.end()
        })
    })
})

test('init ArtefactServer (pataka)', t => {
  console.time('PatakaServer startup')

  ArtefactServer({ pataka: true, dev: true })
    .then(server => {
      console.timeEnd('PatakaServer startup')
      t.ok(server, 'starts pataka server!')

      server.close()
        .then(t.end)
    })
})
